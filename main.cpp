/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <pthread.h>

using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
data_t *pRdest, *pGdest, *pBdest;
data_t *pDstImage; // Pointer to the new image pixels
data_t Rmin, Rmax, Bmin, Bmax, Gmin, Gmax;
uint width, height; // Width and height of the image
uint nComp; // Number of image components
struct timespec tStart, tEnd;
double dElapsedTimes;
float low = 0, high = 255;
uint repeticiones=5;
uint numhilos=128; //8 procesadores*8nucleos*2(Hyperthreading)
void* Task(void*);



int main() {
	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	

	


	

	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;



	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
	}

	/************************************************
	 * FIXME: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */

	
	//Establecemos min y max
	Rmin = high;
	Rmax = low;
	Gmin = high;
	Gmax = low;
	Bmin = high;
	Bmax = low;
	//Calculo del total de cada color
	for (uint i = 0; i < width * height; i++){
			if(*(pRsrc+i)>Rmax) Rmax=*(pRsrc+i);
			if(*(pRsrc+i)<Rmin) Rmin=*(pRsrc+i);
			if(*(pGsrc+i)>Gmax) Gmax=*(pGsrc+i);
			if(*(pGsrc+i)<Gmin) Gmin=*(pGsrc+i);
			if(*(pBsrc+i)>Bmax) Bmax=*(pBsrc+i);
			if(*(pBsrc+i)<Bmin) Bmin=*(pBsrc+i);
		}

	//Ejecutamos el algoritmo repeticiones veces.
	for(uint j=0;j<repeticiones;j++){
		pthread_t thread[numhilos];
		int a[numhilos];
		for (uint i = 0; i < numhilos; i++){
        	a[i]=i*width*height/numhilos;
        	if (pthread_create(&thread[i], NULL, Task, &a[i]) != 0){
            	fprintf(stderr, "ERROR: Creating thread %d\n", i);
            	return EXIT_FAILURE;}
    	}
		for(uint j=0; j<numhilos;j++){
            if(pthread_join(thread[j],NULL)){
                fprintf(stderr,"ERROR\n");
                return 2;}
        }
	}
	
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
	}

	// Show the elapsed time
	dElapsedTimes = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimes += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time	: %f s.\n", dElapsedTimes);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);

	return 0;
}

void* Task(void* n){
	uint inicio=*(int *)n;
	uint fin=height*width/numhilos + inicio;
	for (uint i = inicio; i < fin; i++){
		//Algoritmo balance de blancos
		*(pRdest + i) = ((*(pRsrc + i)-Rmin)/(Rmax - Rmin))*Rmax+Rmin;
		*(pGdest + i) = ((*(pGsrc + i)-Gmin)/(Gmax-Gmin))*Gmax+Gmin;
		*(pBdest + i) = ((*(pBsrc + i)-Bmin)/(Bmax-Bmin))*Bmax+Bmin;

		//Ifs para controlar la saturacion (Rango 0 255)
		if(*(pRdest + i)>high){*(pRdest + i)=high;}
		if(*(pRdest + i)<low){*(pRdest + i)=low;}
		if(*(pGdest + i)>high){*(pGdest + i)=high;}
		if(*(pGdest + i)<low){*(pGdest + i)=low;}
		if(*(pBdest + i)>high){*(pBdest + i)=high;}
		if(*(pBdest + i)<low){*(pBdest + i)=low;}
	}
	return 0;
}
